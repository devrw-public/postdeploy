#!/usr/bin/env bash

apt-get update
apt-get -y install ansible git unzip wget jq
#git clone https://gitlab.com/rwojciechowski/postdeploy.git
cd postdeploy
wget https://gitlab.com/devrw-public/postdeploy/-/raw/main/bws-aarch64-unknown-linux-gnu-1.0.0.zip
unzip bws-aarch64-unknown-linux-gnu-1.0.0.zip
chmod +x bws
cp bws /usr/bin/
/usr/bin/ansible-galaxy collection install community.general
/usr/bin/ansible-galaxy collection install ansible.posix
/usr/bin/ansible-galaxy collection install ansible.netcommon
/usr/bin/ansible-pull -U https://rwojciechowski:`bws secret get 1bffc2e2-a729-4cae-88d6-b1180115b46c | jq -r '.value'`@gitlab.com/rwojciechowski/iac.git -C main -i /root/.ansible/pull/$(hostname)/inventory/hosts ansible-pull.yml -f || /usr/bin/ansible-pull -U https://rwojciechowski:`bws secret get 1bffc2e2-a729-4cae-88d6-b1180115b46c | jq -r '.value'`@gitlab.com/rwojciechowski/iac.git -C main -i /root/.ansible/pull/$(hostname)/inventory/hosts ansible-pull.yml -f