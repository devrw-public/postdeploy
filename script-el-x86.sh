#!/usr/bin/env bash
source /root/.bashrc
[ -z "$BWS_ACCESS_TOKEN" ] && exit 1
dnf -y install epel-release
dnf -y install ansible git unzip wget jq python3-dns tar
cd postdeploy
wget https://gitlab.com/devrw-public/postdeploy/-/raw/main/bws-x86_64-unknown-linux-gnu-1.0.0.zip
unzip bws-x86_64-unknown-linux-gnu-1.0.0.zip
chmod +x bws
cp bws /usr/bin/
cd /opt
mkdir pushover && cd pushover
wget https://gitlab.com/devrw-public/postdeploy/-/raw/main/env.tar.gz
tar -zxvf env.tar.gz
/usr/bin/ansible-galaxy collection install community.general
/usr/bin/ansible-galaxy collection install ansible.posix
/usr/bin/ansible-galaxy collection install ansible.netcommon
/usr/bin/ansible-pull -U https://rwojciechowski:`bws secret get 1bffc2e2-a729-4cae-88d6-b1180115b46c | jq -r '.value'`@gitlab.com/rwojciechowski/iac.git -C main -i /root/.ansible/pull/$(hostname)/inventory/hosts ansible-pull.yml -f || /usr/bin/ansible-pull -U https://rwojciechowski:`bws secret get 1bffc2e2-a729-4cae-88d6-b1180115b46c | jq -r '.value'`@gitlab.com/rwojciechowski/iac.git -C main -i /root/.ansible/pull/$(hostname)/inventory/hosts ansible-pull.yml -f
/usr/bin/ansible-pull -U https://rwojciechowski:`bws secret get 1bffc2e2-a729-4cae-88d6-b1180115b46c | jq -r '.value'`@gitlab.com/rwojciechowski/iac.git -C main -i /root/.ansible/pull/$(hostname)/inventory/hosts ansible-pull.yml -f || /usr/bin/ansible-pull -U https://rwojciechowski:`bws secret get 1bffc2e2-a729-4cae-88d6-b1180115b46c | jq -r '.value'`@gitlab.com/rwojciechowski/iac.git -C main -i /root/.ansible/pull/$(hostname)/inventory/hosts ansible-pull.yml -f
dnf -y update
reboot